/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mesaya.washingmachine;

/**
 *
 * @author ADMIN
 */
public class MediumWashingMachine extends WashingMachine {

    public MediumWashingMachine(int coin, String temp) {
        super(14, coin, temp);
    }

    @Override
    public void welcomWashing() {
        System.out.println("WASHING MACHINE MEDIUM KARN");

    }

    @Override
    public void Washing() {
        System.out.println("Weight : " + weight +" KG "+  "\nCoin : " + coin + "\nTemp : " + temp);
    }

}

