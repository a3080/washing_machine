/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mesaya.washingmachine;

/**
 *
 * @author ADMIN
 */
public class TestWashing {

    public static void main(String[] args) {

        WashingMachine washingMachine = new WashingMachine(0, 0, "start");
        washingMachine.welcomWashing();
        washingMachine.Washing(25);

        System.out.println("-----------------------------");

        SmallWashingMachine small = new SmallWashingMachine(40, "Hot");
        small.welcomWashing();
        small.Washing();
        small.setTemp("Normal");
        System.out.println("-----------------------------");

        small.Washing();
        System.out.println("-----------------------------");
        small.Washing("cool", 30);

        System.out.println("-----------------------------");

        MediumWashingMachine medium = new MediumWashingMachine(60, "Normal");
        medium.welcomWashing();
        medium.Washing();
        System.out.println("-----------------------------");

        LargeWashingMachine large = new LargeWashingMachine(100, "cool");
        large.welcomWashing();
        large.Washing();
        System.out.println("-----------------------------");

    }

}
